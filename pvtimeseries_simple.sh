#!/bin/bash
# write a time series form a given list of Solution files (given as argument, for example 'project*.pvtu')
# _Solution_ must be  in the file
# Example: pvtimeseries 'figi_Solution_*.pvtu'
#
# <?xml version="1.0"?>
# <VTKFile type="Collection" version="0.1"
#          byte_order="LittleEndian"
#          compressor="vtkZLibDataCompressor">
#   <Collection>
#      <DataSet timestep="0000000.0000000" file="figi_Solution_0000000.0000000_Combined.pvtu"/>
#      <DataSet timestep="0000000.5000000" file="figi_Solution_0000000.5000000_Combined.pvtu"/>
#      <DataSet timestep="0000001.0000000" file="figi_Solution_0000001.0000000_Combined.pvtu"/>
#      <DataSet timestep="0000001.5000000" file="figi_Solution_0000001.5000000_Combined.pvtu"/>
#      <DataSet timestep="0000002.0000000" file="figi_Solution_0000002.0000000_Combined.pvtu"/>
# ....
#   </Collection>
# </VTKFile>


files=$*
nfiles=${#files[@]}
echo '===================================================================='
if [ $nfiles -eq 0 ]; then
  echo 'NO FILES FOUND!!!' 
  echo '===================================================================='
  exit
fi


#pvdfile=$projectname'_TimeSeries_'$starttime'-'$endtime'.pvd'
pvdfile='TimeSeries.pvd'


echo '<?xml version="1.0"?> ' >$pvdfile
echo '<VTKFile type="Collection" version="0.1" ' >>$pvdfile
echo '         byte_order="LittleEndian" ' >> $pvdfile
echo '         compressor="vtkZLibDataCompressor"> ' >> $pvdfile
echo '  <Collection> ' >> $pvdfile

time=0
for file in ${files[@]}
do
  time=$((time+1))  
  echo file: $file time: $time 
  echo '     <DataSet timestep="'$time'" file="'$file'"/>' >> $pvdfile
done

echo '  </Collection> ' >> $pvdfile
echo '</VTKFile> ' >> $pvdfile

echo '===================================================================='
echo 'TIMESERIES WRITTEN TO : ' $pvdfile
echo '===================================================================='
