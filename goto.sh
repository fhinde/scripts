#!/bin/bash
#
# execute this script with . ~/goto.sh number
#
workpath=/marconi_work/FUA32_AUGJOR/fhindenl
wp=$workpath"/*/*"


num=$(printf "%03d" $1)
ddd=`ls -d $wp | grep 'runknl_'$num'_'`
echo "     "
echo "==> change to directory: "
echo "     " $ddd
echo "     "

cd $ddd

