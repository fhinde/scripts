#!/bin/bash
# write a time series form a given list of Solution files (given as argument, for example 'project*.pvtu')
# _Solution_ must be  in the file
# Example: pvtimeseries 'figi_Solution_*.pvtu'
#
# <?xml version="1.0"?>
# <VTKFile type="Collection" version="0.1"
#          byte_order="LittleEndian"
#          compressor="vtkZLibDataCompressor">
#   <Collection>
#      <DataSet timestep="0000000.0000000" file="figi_Solution_0000000.0000000_Combined.pvtu"/>
#      <DataSet timestep="0000000.5000000" file="figi_Solution_0000000.5000000_Combined.pvtu"/>
#      <DataSet timestep="0000001.0000000" file="figi_Solution_0000001.0000000_Combined.pvtu"/>
#      <DataSet timestep="0000001.5000000" file="figi_Solution_0000001.5000000_Combined.pvtu"/>
#      <DataSet timestep="0000002.0000000" file="figi_Solution_0000002.0000000_Combined.pvtu"/>
# ....
#   </Collection>
# </VTKFile>


files=$*
nfiles=${#files[@]}
echo '===================================================================='
if [ $nfiles -eq 0 ]; then
  echo 'NO FILES FOUND!!!' 
  echo '===================================================================='
  exit
fi

firstfile=${files[0]%.*}


fileparts=(`echo $firstfile|sed -e 's/_/ /g'`)
nfileparts=${#fileparts[@]}
timepos='-1'
for (( ii=0; ii<$nfileparts; ii++ ))
do
  if [[ "${fileparts[$ii]}" =~ "." ]]; then
    timepos=$((ii+1))
  fi
done
if [ $timepos -eq '-1' ]; then
  echo 'NO Time argument found in file, use simple counter'
  starttime='1'
  endtime=$nfiles
else
  starttime=`echo ${files[0]%.*} | cut -d '_' -f$timepos `
  endtime=`echo ${files[$((nfiles-1))]%.*} | cut -d '_' -f$timepos `
fi

projectname=`echo $firstfile | cut -d '_' -f1-$((timepos-1))`

echo 'PROJECTNAME: ' $projectname

#pvdfile=$projectname'_TimeSeries_'$starttime'-'$endtime'.pvd'
pvdfile=$projectname'_TimeSeries.pvd'


echo '<?xml version="1.0"?> ' >$pvdfile
echo '<VTKFile type="Collection" version="0.1" ' >>$pvdfile
echo '         byte_order="LittleEndian" ' >> $pvdfile
echo '         compressor="vtkZLibDataCompressor"> ' >> $pvdfile
echo '  <Collection> ' >> $pvdfile

time=0
for file in ${files[@]}
do
  if [ $timepos -eq '-1' ]; then
    time=$((time+1))  
  else
    time=`echo ${file%.*} | cut -d '_' -f$timepos `
  fi
  echo file: $file time: $time 
  echo '     <DataSet timestep="'$time'" file="'$file'"/>' >> $pvdfile
done

echo '  </Collection> ' >> $pvdfile
echo '</VTKFile> ' >> $pvdfile

echo '===================================================================='
echo 'TIMESERIES WRITTEN TO : ' $pvdfile
echo '===================================================================='
