#!/bin/bash
#######################################################################################
# bash script to plot the jobs that the current user has run in the last x days       #
# run with:                                                                           #
#                                                                                     #
# >  ./lastruns.sh [number of days]                                                   #
#                                                                                     #
# if nothing is specified default is 4 days...                                        #
# (script must be set executable)                                                     #
#                                                                                     #
# additionally it plots the current running and queued jobs (& estimated start time)  #
# as well as the local time on the machine                                            #
#                                                                                     #
# you can put the script in the home directory and set an alias:                      #
# alias lastruns='~/lastruns.sh $1'                                                   #
# then you just use the command                                                       #
#                                                                                     #
# > lastruns [number of days]                                                         #
#                                                                                     #
# Written by Florian Hindenlang                                                       #
#######################################################################################

day2=`date +'%F'`  #TODAY
if [ $# -eq 0 ]
  then
  lastdays='4'
else
  lastdays=$1
fi
day1=`date -d "$lastdays days ago" +'%F' ` #format yyyy-mm-dd

curruser=`echo $USER`

echo "----------------------------"
echo " RUNS IN THE LAST " $lastdays " DAYS:"
echo "----------------------------"
sacct -u $curruser -S $day1-00:00:00 -E $day2-23:59:59 --format "JobID,Account%10,JobName%20,NNodes%5,NCPUs%6,Elapsed,Start%21,End%21,CPUtimeRAW"  | sed '/  00:00:00/d' |awk '{
    jobid=$1;
    acct=$2;
    nnodes=$4; 
    ncpu=$5; 
    tcpu=$9; 
    if(index(jobid, ".") == 0) 
      {if(ncpu =="NCPUS")
         {printf "%s\b\b\b\b\b\b\b\b\b\b\b %10s %10s %10s\n",$0,"Node-h","+n-h fua23 ","+n-h fua33"} 
       else 
         {if (ncpu=="------") 
            {printf "%s\b\b\b\b\b\b\b\b\b\b\b %10s %10s %10s\n",$0,"----------","----------","----------"} 
          else 
            {if(ncpu ==0) 
               {nodeh=0} 
             else 
               { nodeh=tcpu/ncpu*nnodes/3600} 
               if(index(acct, "fua22") != 0) 
                 {sum1 += nodeh; i++}
               else
                 {sum2 += nodeh; i++}
             printf "%s\b\b\b\b\b\b\b\b\b\b\b %10.1f %10.1f %10.1f\n", $0, nodeh, sum1,sum2
            }
         }
      }
   }'
echo "------------ -------------------- ------ ------- ---------- ------------------------ ------------------------  -------- --------"
echo " "
echo "---------------------------------"
echo " JOBS CURRENTLY RUNNING / QUEUED: "
echo "---------------------------------"
squeue -o "%.7i %.12a %.20j %.2t %.10M %.10L %.6D %.10Q %.20S %.25R" -S "Mp"  -u $curruser
echo " "
echo "-------------------"
echo 'CURRENT LOCAL TIME:'
echo "-------------------"
date +"%F T %R:%S"

