#!/usr/bin/python
# -*- coding: utf-8 -*-

import os,fnmatch
import sys
import shutil
import argparse
import math

########################################################################################################

# MAIN PROGRAM

########################################################################################################
parser = argparse.ArgumentParser(description='# Tool to submit a chain of jobs,\n'
                                             '# given the number of total jobs and the jobscript\n'
                                             '# example execution command:\n'
                                             '# ./submit_jobchain.py -njobs 4  jobscript.sh',\
                                 formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('jobscript',type=str, 
                                 help='Name of jobscript file, mandatory, last argument')
parser.add_argument('-njobs', type=int,   
                                 help=" number of jobs to submit, mandatory",required=True)
parser.add_argument('-after', type=int, default=-1,  
                                 help="start first job after this job ID, defualt=-1, first job is directly submitted ")
parser.add_argument('-system', type=str, default='slurm',   
                                 help='which batch system you are running, changes the way \n'
                                      'jobs are submitted and the job id is parsed \n'
                                      'Default: “slurm“, others: "loadleveler", "PBS" ')
parser.add_argument('-dryrun' , type=int, default='0',  
                                 help='only write commands to screen without submitting them, (off:0, on=1, default: 0)')

args = parser.parse_args()


if(args.system == "loadleveler") :
  submitcall="llsubmit"
  depcall   ="--dependency=afterany:"
elif(args.system == "PBS") :
  submitcall="qsub"
  depcall   ="-W depend=afterany:"
elif(args.system == "slurm") :
  submitcall="sbatch"
  depcall   ="--dependency=afterany:"
else:
  print('the batch system "%s" is not yet implemented for this script!'% (args.system))
  sys.exit(100)

print( " " )
print(  "┌───────────────────┬──────────────────────────────────────────┐")
print(  "│ jobscript         │ %40s │" % (args.jobscript) )
print(  "│ -njobs            │                                   %6d │" % (args.njobs) )
print(  "│ -batch system     │ %40s │" % (args.system) )
print(  "│  -> submitcall    │ %40s │" % (submitcall) )
print(  "│  -> depcall       │ %40s │" % (depcall) )
print(  "│ -dryrun           │                                   %6d │" % (args.dryrun) )
print(  "└───────────────────┴──────────────────────────────────────────┘")

if (not os.path.isfile(args.jobscript)) :
  print( "    ... jobscript not found: %s, stopping script" % (args.jobscript ))
  sys.exit(100)
else:
  print( "    ... jobscript found: %s" % (args.jobscript ))
  

dosubmit=  ( not( args.dryrun == 1) )

for ijob in range(1,args.njobs+1):
  print("===>  submitting job %6d" % (ijob ))
  if(ijob == 1) :
    if(args.after == -1) :
      cmd=("%s %s " %(submitcall,args.jobscript))
    else:
      cmd=("%s %s%s %s" %(submitcall,depcall,args.after,args.jobscript))
  else:
    cmd=("%s %s%s %s" %(submitcall,depcall,lastjobid,args.jobscript))
  print("      >  %s" % cmd)
  if(not dosubmit) : 
    cmd=("echo You submitted jobid_dryrun.%06d" %(10*ijob)) # write artificial submit command output jobid
  os.system(cmd+" 2>std.err 1>std.out")
  stdout=open("std.out",'r').readlines()
  stderr=open("std.err",'r').readlines()
  success= True
  for line in stderr :
    success= False
    sys.stdout.write("ERROR OCCURED DURING EXECUTING COMMAND: %s" % line)
  if(not success) : 
    sys.exit(100)
  else :
    line = stdout[0] #first line
    #machine depended parsing of the submit output
    if(args.system == "loadleveler") :
      lastjobid=line.strip().split(" ")[-1]
    elif(args.system == "PBS") :
      lastjobid=line.strip().split(" ")[-1]
    elif(args.system == "slurm") :
      lastjobid=line.strip().split(" ")[-1]
 
  print('  ...found ID of last job: "%s" ' %(lastjobid))
#end for

sys.exit(0)
