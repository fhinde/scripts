#!/bin/bash
# Submit a chain of batch jobs with dependencies
#
# Number of jobs to submit:
NR_OF_JOBS=$1
# Batch job script:
JOB_SCRIPT=$2
echo "Submitting job chain of ${NR_OF_JOBS} jobs for batch script ${JOB_SCRIPT}:"
JOBID=$(sbatch ${JOB_SCRIPT} 2>&1 | awk '{print $(NF)}')
echo "  " ${JOBID}
I=1
while [ ${I} -lt ${NR_OF_JOBS} ]; do
JOBID=$(sbatch --dependency=afterany:${JOBID} ${JOB_SCRIPT} 2>&1 ) | awk '{print $(NF)}')
echo "  " ${JOBID}
let I=${I}+1
done 
