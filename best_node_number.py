#!/usr/bin/python
# -*- coding: utf-8 -*-

import argparse
import math

########################################################################################################

# MAIN PROGRAM

########################################################################################################
parser = argparse.ArgumentParser(description='# Tool to find out best number of nodes,\n'
                                             '# given the number of elements and the number of cores per node\n'
                                             '# example execution command:\n'
                                             '# ./best_node_number.py -rpn 68 -ne 832 -nez 17  -ploss 4',\
                                 formatter_class=argparse.RawTextHelpFormatter)
parser.add_argument('-rpn', type=int,   
                   help=" number of ranks per node",required=True)
parser.add_argument('-ne', type=int,  
                   help=" global number of elements",required=True )
parser.add_argument('-nn',type=int,default=64, 
                   help=" maximum number of nodes to be scanned (default=64)")
parser.add_argument('-nez', type=int, default=1, 
                   help=" factor onto global elements (default=1)" )
parser.add_argument('-ploss', type=int,default=5,  
                   help=" max allowed performance loss \n"
                        " in percent (default=5)" )

args = parser.parse_args()

ne=args.ne*args.nez

print( " " )
print(  "┌───────────────────┬─────────────┐")
print(  "│ #ranks/node       │    %8d │" % (args.rpn) )
if (args.nez==1) :
  print("│ #elements         │  %10d │" % (ne) )
else:
  print("│ #elements         │  %10d │ (= %5d *%4d )" % (ne,args.ne,args.nez) )
#end if
print(  "│ allowed perf.loss │       %3d %% │" % (args.ploss) )
print(  "│ maximum #nodes    │    %8d │" % (args.nn) )
print(  "└───────────────────┴─────────────┘")

results = []
bestnn = []
bestnranks = []
bestne_rank= []
bestploss  = []

for nn in range(1,args.nn):
  nranks=nn*args.rpn
  ne_rankf = float(ne)/float(nranks)
  ne_rank = math.ceil(ne_rankf)
  #stop if #elem/rank <1
  if ((ne_rankf -1.0)< -1.0e-08 ) :
    break
  #end if

  # number of ranks having ne_rank-1 elements =x, nranks-x with ne_rank elements
  # ne = x*(ne_rank-1)+(nranks-x)*ne_rank 
  #    = nranks*ne_rank -x 
  # x  = nranks*ne_rank - ne
  x = nranks*ne_rank - ne
  xovernranks=float(x)/float(nranks)*100
  perf_loss=xovernranks/float(ne_rank)
  if(math.ceil(perf_loss) <= args.ploss) :
    results.extend(["%6d │ %8d │ %8d │ %8d │ %7.3f %%" % (nn,nranks,x,ne_rank,perf_loss)])
    bestnn.extend([nn])
    bestnranks.extend([nranks])
    bestne_rank.extend([ne_rank])
    bestploss.extend([perf_loss])
  #end if
#end for

nr = len(results)
if(nr > 0 ) :
  mapr=bestnn
  pltmp=bestploss
  for i in range(nr) :
    minpl = args.ploss
    for j in range(nr) : 
      if (pltmp[j] < minpl):
        minpl = pltmp[j]
        minpos = j
      #end if
    #end for
    pltmp[minpos]=100. # take it out
    mapr[i]=minpos
  #end for

    
  print( " " )
  print( " Performance loss due to imbalance between full ranks with #el/rank and 'bad' ranks with #el/rank-1")
  print( "  perf.loss= 1/(#el/rank)*Ratio, ratio=#badranks/#allranks ")
  print( "  we only show nnodes with perf. loss < input ploss %d %% " %(args.ploss) )
  print( " " )
  print( " ..number of nodes increasing                                      ..best ranking" )
  print("┌────────┬──────────┬──────────┬──────────┬───────────┐   ┌───────┬────────┬──────────┬──────────┬──────────┬───────────┐")
  print("│ #nodes │#allranks │#badranks │ #el/rank │ perf.loss │   │ Place │ #nodes │#allranks │#badranks │ #el/rank │ perf.loss │")
  print("├────────┼──────────┼──────────┼──────────┼───────────┤   ├───────┼────────┼──────────┼──────────┼──────────┼───────────┤")
  for i in range(nr) :
    j=mapr[i]
    print("│ %s │   │  %4d │ %s │" %(results[i],i+1,results[j] ))
  #end for
  print("└────────┴──────────┴──────────┴──────────┴───────────┘   └───────┴────────┴──────────┴──────────┴──────────┴───────────┘")
  print( " " )
else :
  print (" " )
  print ("==> NO CONFIGURATION FOUND FOR RATIO %d %% !!!" % args.ratio)
#end if
